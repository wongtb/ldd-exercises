/* **************** LF331:1.6 s_03/lab1_chrdrv.c **************** */
/*
 * The code herein is: Copyright the Linux Foundation, 2011
 *
 * This Copyright is retained for the purpose of protecting free
 * redistribution of source.
 *
 *     URL:    http://training.linuxfoundation.org
 *     email:  trainingquestions@linuxfoundation.org
 *
 * The primary maintainer for this code is Jerry Cooperstein
 * The CONTRIBUTORS file (distributed with this
 * file) lists those known to have contributed to the source.
 *
 * This code is distributed under Version 2 of the GNU General Public
 * License, which you should have received with the source.
 *
 */
/* 
Sample Character Driver 
@*/

#include <linux/module.h>	/* for modules */
#include <linux/fs.h>		/* file_operations */
#include <linux/init.h>		/* module_init, module_exit */

#include <linux/slab.h>		/* kmalloc */
#include <linux/cdev.h>		/* cdev utilities */
#include <linux/uaccess.h>	/* copy_(to,from)_user */

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Brian Wong");
MODULE_DESCRIPTION("Lab 3-1");
MODULE_VERSION("0.1"); 


int myIntParam = 1; 			// default value
module_param(myIntParam, int, 0000); 	// declare as input parameter

static int __init my_init(void)
{
	printk(KERN_INFO "Loading Lab 3-1...\n");
	printk(KERN_INFO "myIntParam: %d\n", myIntParam); 
	return 0;
}

static void __exit my_exit(void)
{
	printk(KERN_INFO "Unloading Lab 3-1\n");
}

module_init(my_init);
module_exit(my_exit);


