
#!/bin/sh

module=lab4-4.ko

node=mycdrv

minor=0



[ "$1" != "" ] && module="$1"

[ "$2" != "" ] && node="$2"



echo loading $module

insmod $module



major=$(cat /proc/devices | grep $node | awk '{print $1}')

echo major number is: $major


echo creating the device node at /dev/$node with minor number=0


mknod /dev/$node c $major $minor
