exercise instructions:
[] Start with sampel_driver.c
[] Keep track of number of times it has been opened
[] Print out upon opening:
[] # of times the device is opened
[] Major and minor numbers
[] Exercise driver by writing a userspace program to read/write from the node using open(), read(), write(), close().  
[] Load the module using ismod
[] Track usage of the module using lsmod

instructions for running:
1. run: mknod -m 666 /dev/mycdrv c 401 0
2. insmod lab4-1
3. dmesg - to check status
4. ./lab4-1_priv
5. dmesg - to check status
6. rmmod lab4-1
