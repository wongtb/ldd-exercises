/* **************** LF331:1.6 s_05/sample_driver.c **************** */
/*
 * The code herein is: Copyright the Linux Foundation, 2011
 *
 * This Copyright is retained for the purpose of protecting free
 * redistribution of source.
 *
 *     URL:    http://training.linuxfoundation.org
 *     email:  trainingquestions@linuxfoundation.org
 *
 * The primary maintainer for this code is Jerry Cooperstein
 * The CONTRIBUTORS file (distributed with this
 * file) lists those known to have contributed to the source.
 *
 * This code is distributed under Version 2 of the GNU General Public
 * License, which you should have received with the source.
 *
 */
/* 
Sample Character Driver 
@*/

#include <linux/module.h>	/* for modules */
#include <linux/fs.h>		/* file_operations */
#include <linux/uaccess.h>	/* copy_(to,from)_user */
#include <linux/init.h>		/* module_init, module_exit */
#include <linux/slab.h>		/* kmalloc */
#include <linux/cdev.h>		/* cdev utilities */

#define MYDEV_NAME "mycdrv"

//static char *ramdisk;
#define ramdisk_size (size_t) (16*PAGE_SIZE)

static dev_t first;
static unsigned int count = 1;
static int my_major = 401, my_minor = 0;
static struct cdev *my_cdev;
static struct class * c1;
static int open_count = 0; 	

static int mycdrv_open(struct inode *inode, struct file *file)
{
	char *ramdisk = kmalloc(ramdisk_size, GFP_KERNEL);
	file->private_data = ramdisk;  
	open_count++; 
	printk(KERN_INFO " OPENING device: %s:", MYDEV_NAME);
	printk(KERN_INFO " Device Major: %d, Minor: %d", imajor(inode), iminor(inode)); 
	printk(KERN_INFO " Open Count: %d ", open_count); 	
	return 0;
}

static int mycdrv_release(struct inode *inode, struct file *file)
{
	char *ramdisk = file->private_data; 
	kfree(ramdisk); 	
	printk(KERN_INFO " CLOSING device: %s:", MYDEV_NAME);
	return 0;
}

static ssize_t
mycdrv_read(struct file *file, char __user * buf, size_t lbuf, loff_t * ppos)
{
	int nbytes;
	char *ramdisk = file->private_data; 

	if ( (lbuf+*ppos) > ramdisk_size ) {
		printk(KERN_INFO "trying to read past end of device," 
		       "aborting because this is just a stub!\n");
		return 0;
	}

	// read last write
	nbytes = lbuf - copy_to_user(buf, ramdisk + *ppos - lbuf, lbuf);
	printk(KERN_INFO "READING function, ramdisk size = %d, nbytes=%d, pos=%d", ramdisk_size, nbytes, (int)*ppos);
	return nbytes;
}

static ssize_t
mycdrv_write(struct file *file, const char __user * buf, size_t lbuf,
	     loff_t * ppos)
{
	char *ramdisk = file->private_data; 
	int nbytes;
	if ( (lbuf+*ppos) > ramdisk_size ) {
		printk(KERN_INFO "trying to read past end of device," 
		       "aborting because this is just a stub!\n");
		return 0;
	}
	nbytes = lbuf - copy_from_user(ramdisk + *ppos, buf, lbuf);
	*ppos += nbytes; // move the position pointer nbytes after write
	printk(KERN_INFO "WRITING function, nbytes=%d, pos=%d", nbytes,
	       (int)*ppos);
	return nbytes;
}

static const struct file_operations mycdrv_fops = {
	.owner = THIS_MODULE,
	.read = mycdrv_read,
	.write = mycdrv_write,
	.open = mycdrv_open,
	.release = mycdrv_release,
};

static int __init my_init(void)
{
	//reserve major and minor numbers
	first = MKDEV(my_major, my_minor);
	register_chrdev_region(first, count, MYDEV_NAME);	// register the my_major = 401 for MYDEV_NAME = mycdrv in kernel
	
	// register device driver
	my_cdev = cdev_alloc();					// allocate with the kernel, how many device #s starting from my_minor=0
	cdev_init(my_cdev, &mycdrv_fops);			// initialize a dev structure
	int err = cdev_add(my_cdev, first, count);		// adds a char device to the system - this does not create /dev/MYDEV_NAME??

	// create device node
	c1 = class_create(THIS_MODULE, MYDEV_NAME); 
	device_create(c1, NULL, first, NULL, MYDEV_NAME);

	// print stuff
	printk(KERN_INFO "//// LOADING LAB 4-2 DRIVER ////"); 	
	printk(KERN_INFO "Succeeded in registering character device: %s\n",
	       MYDEV_NAME);
	return 0;
}

static void __exit my_exit(void)
{
	//remove node
	device_destroy(c1, first);
	class_destroy(c1); 
	printk(KERN_INFO "Device node removed: %s", MYDEV_NAME); 	

	// unregister driver
	cdev_del(my_cdev);
	unregister_chrdev_region(first, count);
	printk(KERN_INFO "Device unregistered");
}

module_init(my_init);
module_exit(my_exit);

MODULE_AUTHOR("Jerry Cooperstein");
MODULE_LICENSE("GPL v2");
